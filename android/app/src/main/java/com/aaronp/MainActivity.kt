package com.aaronp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var number1 : EditText
    lateinit var number2 : EditText
    lateinit var compare : Button
    lateinit var result1 : TextView
    lateinit var result2 : TextView
    val TAG:String = "MainActivity"

    var winner : Int = 0
    var winner2 : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        number1 = findViewById(R.id.editN1)
        number2 = findViewById(R.id.editN2)
        compare = findViewById(R.id.btn)
        result1 = findViewById(R.id.tv)
        result2 = findViewById(R.id.tv2)

        compare.setOnClickListener {

            Toast.makeText(this, "Comparando", Toast.LENGTH_SHORT).show()
            compareNumbers()
        }
    }
    fun compareNumbers(){

        var num1 : Int = 0
        var num2 : Int = 0

        num1 = number1.text.toString().toInt()
        num2 = number2.text.toString().toInt()


        if(num1 > num2){
            winner = num1
            result2.setText(R.string.first)
            Log.v(TAG, "True")
        } else {
            winner = num2
            result2.setText(R.string.second)
            Log.v(TAG, "False")
        }
        result1.setText(winner.toString())
    }
}